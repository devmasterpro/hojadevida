import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  LoadingController,
  ModalController } from '@ionic/angular';

// Modals
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { UsersService } from 'src/app/services/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage implements OnInit{
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/ucc.jpg';
  // DEFINIMOS LA VARIABLE DE FORMULARIO
  public onEditForm: FormGroup;
  // DEFINIMOS LA VARIABLE DE LA VISTA
  item={}

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public userService: UsersService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,

  ) {
    this.select_info()

    this.onEditForm = this.formBuilder.group({
      'full_name': [null, Validators.compose([
        Validators.required
      ])],
      'email': [null],
      'birthdate': [null, Validators.compose([
        Validators.required
      ])],
      'type_identification': [null, Validators.compose([
        Validators.required
      ])],
      'number_identification': [null, Validators.compose([
        Validators.required
      ])],
      'civil_state': [null, Validators.compose([
        Validators.required
      ])],
      'position': [null, Validators.compose([
        Validators.required
      ])],
      'description_position': [null, Validators.compose([
        Validators.required
      ])],
      'address': [null, Validators.compose([
        Validators.required
      ])],
      'country': [null, Validators.compose([
        Validators.required
      ])],
      'city': [null, Validators.compose([
        Validators.required
      ])]
    });

  }

  ngOnInit(){
  }


  ionViewWillEnter() {
    this.select_info()
    this.menuCtrl.enable(true);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  // FUNCION QUE LLAMA LOS DATOS DEL USUARIO AUTENTICADO PARA MOSTARLOS EN LA VISTA
  async select_info(id=localStorage.getItem('user_id')) {
    this.userService.InformationUserCV(id)
    .then(
      (response : any) => {
        if(response.rows.length > 0){
          this.item = response.rows.item(0);
        }
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    );
  }


  // FUNCION QUE ACTUALIZA LOS DATOS
  async update_data(){
    var information = this.onEditForm.value;

    this.userService.UpdateCV(information,localStorage.getItem('user_id'))
    .then(
      (res : any) => {
      }
    )
    .catch(
      (err : any) => {
        console.log(err)
        this.error()        
      }
    ); 
  }

  async save_data(){
    // LLamamos la funcion que actualiza los datos
    this.update_data()

    const loader = await this.loadingCtrl.create({
      duration: 500
    });
    loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        // cssClass: 'bg-profile',
        message: 'Hoja de vida Actualizada!',
        duration: 1000,
        position: 'bottom'
      });

      toast.present();
      this.navCtrl.navigateForward('home-results');
    });
  }

  // TOAST DE ERROR
  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Algo ha ocurrido en el servicio :(',
      buttons: ['OK']
    });

    await alert.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  // ESTA FUNCION ES DEL TEMPLATE
  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

}
