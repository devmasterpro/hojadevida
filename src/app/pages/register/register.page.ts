import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public onRegisterForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    public userService: UsersService
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  async error() {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      message: 'Algo ha ocurrido en el servicio :(',
      buttons: ['OK']
    });

    await alert.present();
  }
  
  async show_data(data) {
    const alert = await this.alertCtrl.create({
      header: 'data',
      message: data,
      buttons: ['OK']
    });

    await alert.present();
  }

  async signUp() {

    const loader = await this.loadingCtrl.create({
      duration: 2000
    });

    loader.present();

    loader.dismiss();

    var credentials = this.onRegisterForm.value;

    this.userService.createUser(credentials)
    .then(
      (res : any) => {
        // CREAMOS UN NUEVO REGISTRO PARA LA TABLA CV
        this.new_register(res.insertId)
      }
    )
    .catch(
      (err : any) => {
        console.log(err)

        this.error()
        
      }
    );  

  }


  new_register(id){
    this.userService.createUserInfo(id)
    .then(
      (response : any) => {
        // ALMACENAMOS LA VARIABLE EN EL LOCALSTORAGE PARA NO ESTAR CONSULTANDO CADA RATO
        localStorage.setItem('user_id', id.toString());
        this.navCtrl.navigateRoot('/edit-profile');
      }
    )
    .catch(
      (err : any) => {
        console.log(err)

        this.error()
        
      }
    ); 
  }

  goToLogin() {
    this.navCtrl.navigateRoot('/');
  }
}
